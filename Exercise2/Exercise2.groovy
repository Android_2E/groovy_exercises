class Main {

    static void main(def args) {
        //Take in exaclty 3 arguments
        if (args.length != 3) {
            println('Less/More arguments than excpected')
            System.exit(1)
        }

        String filepath = args[0]
        String arg1 = args[1]
        String arg2 = args[2]

        def file = new File (filepath)

        //Check if file is correct and readable
        if (!file.exists() || !file.canRead()) {
            println('Error opening file, exiting...')
            System.exit(2)
        }

        //Check if file is empty
        if (file.length() == 0) {
            println('Specified file is empty, exiting...')
            System.exit(3)
        }

        def newfile = file.text.replace(arg1, arg2)
        file.text = newfile
        println('Completed...')
    }

}
