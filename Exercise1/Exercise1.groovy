class Main {

    static void main(def args) {
        //Take in exactly 2 arguments
        if (args.length != 2) {
            println('Less/More arguments than excpected')
            System.exit(1)
        }

        File file = new File(args[0])

        //Check for existance of file
        if (!file.exists() || !file.canRead()) {
            println('Error opening file, exiting...')
            System.exit(2)
        }
        def lines = file.readLines()
        int count = 0

        //Check if file is empty
        if (file.length() == 0) {
            println('Specified file is empty, exiting...')
            System.exit(3)
        }

        for (line in lines) {
            String[] words = line.split('    ')
            for (word in words) {
                if (word == args[1]) {
                    count += 1
                }
            }
        }
        println(count)
    }

}
